package model.conexion;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class conexion {

	 static Connection conn = null;


	    public static void conexion() {

	        try {
	            Class.forName("com.mysql.cj.jdbc.Driver");
	            conn = DriverManager.getConnection("jdbc:mysql://192.168.1.44:3306?useTimezone=true&serverTimezone=UTC","remote","041118AV");
	            if (conn != null) {
	                System.out.println("Servidor conectado");
	            }
	        }catch(SQLException e){
	            System.out.println(e);
	        }catch(ClassNotFoundException e){
	            System.out.println(e);
	        }catch(Exception e){
	            System.out.println(e);
	        }


	    }
	    
	    public void closeConnection() {
			
			try {
				
				conn.close();
				System.out.println("Server Disconnected");
				
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.println("Error cerrando conexion");
			}
		}

	    public Connection getConnection(){
	        return conn;
	    }

	    public void desconectar(){
	        conn = null;
	    }
}
