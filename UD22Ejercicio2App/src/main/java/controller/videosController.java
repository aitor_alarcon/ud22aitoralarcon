package controller;

import model.dto.Clientes;
import model.service.ClientesServ;
import views.ventanaPrincipal;
import views.ventanaRegistrarVideos;

public class videosController {

	private ClientesServ clientesServ;
	private ventanaPrincipal miVentanaPrincipal;
	private ventanaRegistrarVideos miVentanaRegistrarVideos;
	
	//Metodos getter Setters de vistas
	public ventanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	public void setMiVentanaPrincipal(ventanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	public ventanaRegistrarVideos getMiVentanaRegistro() {
		return miVentanaRegistrarVideos;
	}
	public void setMiVentanaRegistro(ventanaRegistrarVideos miVentanaRegistro) {
		this.miVentanaRegistrarVideos = miVentanaRegistro;
	}
	public ClientesServ getPersonaServ() {
		return clientesServ;
	}
	public void setPersonaServ(ClientesServ clientesServ) {
		this.clientesServ = clientesServ;
	}
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarVentanaRegistro() {
		miVentanaRegistrarVideos.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarPersona(Clientes miCliente) {
		clientesServ.validarRegistro(miCliente);
	}
	
	public Clientes buscarPersona(String codigoCliente) {
		return clientesServ.validarConsulta(codigoCliente);
	}
	
	public void modificarPersona(Clientes miCliente) {
		clientesServ.validarModificacion(miCliente);
	}
	
	public void eliminarPersona(String codigo) {
		clientesServ.validarEliminacion(codigo);
	}


}


