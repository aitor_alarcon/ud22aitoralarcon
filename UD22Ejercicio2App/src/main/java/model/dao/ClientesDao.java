package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import model.conexion.conexion;
import model.dto.Clientes;

public class ClientesDao {
	
	public void registrarCliente(Clientes miCliente)
	{
		conexion conex= new conexion();
		
		try {
			Statement st = conex.getConnection().createStatement();
			String sql= "INSERT INTO cliente VALUES ('"+miCliente.getIdCliente()+"', '"
					+miCliente.getNombreCliente()+"', '"+miCliente.getApellidoCliente()+"', '"
					+miCliente.getDireccionCliente()+"', '"+miCliente.getdniCliente()+"', '"
					+miCliente.getfechaCliente()+"');";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente","InformaciÃ³n",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}

	public Clientes buscarCliente(int codigo) 
	{
		conexion conex= new conexion();
		Clientes cliente= new Clientes();
		boolean existe=false;
		try {
			String sql= "SELECT * FROM cliente where id = ? ";
			PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
			consulta.setInt(1, codigo);
			ResultSet res = consulta.executeQuery();
			while(res.next()){
				existe=true;
				Clientes.setIdCliente(Integer.parseInt(res.getString("id")));
				Clientes.setNombreCliente(res.getString("nombre"));
				Clientes.setApellidoCliente(res.getString(res.getString("apellido")));
				Clientes.setDireccionCliente(res.getString("direccion"));
				Clientes.setdniCliente(Integer.parseInt(res.getString("dni")));
				Clientes.setfechaCliente(Integer.parseInt(res.getString("fecha")));
			 }
			res.close();
			conex.desconectar();
			System.out.println(sql);
					
			} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
			}
		
			if (existe) {
				return cliente;
			}
			else return null;				
	}

	public void modificarCliente(Clientes miCliente) {
		
		conexion conex= new conexion();
		try{
			String consulta="UPDATE cliente SET id= ? ,nombre = ? , apellido=? , direccion=? , dni= ?, , fecha= ? WHERE id= ? ";
			PreparedStatement estatuto = conex.getConnection().prepareStatement(consulta);
			
            estatuto.setInt(1, miCliente.getIdCliente());
            estatuto.setString(2, miCliente.getNombreCliente());
            estatuto.setString(3, miCliente.getApellidoCliente());
            estatuto.setString(4, miCliente.getDireccionCliente());
            estatuto.setInt(5,miCliente.getdniCliente());
            estatuto.setInt(6, miCliente.getfechaCliente());
            estatuto.executeUpdate();
            
          JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ","ConfirmaciÃ³n",JOptionPane.INFORMATION_MESSAGE);
          System.out.println(consulta);
         

        }catch(SQLException	 e){

            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Error al Modificar","Error",JOptionPane.ERROR_MESSAGE);

        }
	}

	public void eliminarPersona(String codigo)
	{
		conexion conex= new conexion();
		try {
			String sql= "DELETE FROM cliente WHERE id='"+codigo+"'";
			Statement st = conex.getConnection().createStatement();
			st.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","InformaciÃ³n",JOptionPane.INFORMATION_MESSAGE);
            System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}

}

