package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ventanaBuscarVideos extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldCodigo;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JLabel lblCliID;
	private JTextField textFieldCliID;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JButton btnCancelar;
	private JLabel lblAdministrarClientes;

	public ventanaBuscarVideos() {
		setTitle("Administrar Videos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 646, 402);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Código");
		lblCodigo.setBounds(38, 105, 56, 16);
		contentPane.add(lblCodigo);
		
		JLabel lblTitulo = new JLabel("Título");
		lblTitulo.setBounds(38, 162, 56, 16);
		contentPane.add(lblTitulo);
		
		JLabel lblDirector = new JLabel("Director");
		lblDirector.setBounds(38, 226, 56, 16);
		contentPane.add(lblDirector);
		
		textFieldCodigo = new JTextField();
		textFieldCodigo.setBounds(98, 101, 116, 24);
		contentPane.add(textFieldCodigo);
		textFieldCodigo.setColumns(10);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(98, 159, 226, 24);
		contentPane.add(textFieldNombre);
		
		textFieldApellido = new JTextField();
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(98, 223, 226, 24);
		contentPane.add(textFieldApellido);
		
		lblCliID = new JLabel("Cli ID");
		lblCliID.setBounds(356, 162, 30, 16);
		contentPane.add(lblCliID);
		
		textFieldCliID = new JTextField();
		textFieldCliID.setColumns(10);
		textFieldCliID.setBounds(399, 159, 178, 24);
		contentPane.add(textFieldCliID);
		
		JButton btnOk = new JButton("Ok");
		btnOk.setBounds(227, 101, 73, 25);
		contentPane.add(btnOk);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(127, 276, 109, 25);
		contentPane.add(btnGuardar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBounds(248, 276, 109, 25);
		contentPane.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(369, 276, 109, 25);
		contentPane.add(btnEliminar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(248, 314, 109, 25);
		contentPane.add(btnCancelar);
		
		lblAdministrarClientes = new JLabel("ADMINISTRAR VIDEOS");
		lblAdministrarClientes.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAdministrarClientes.setBounds(211, 39, 240, 32);
		contentPane.add(lblAdministrarClientes);
	}

}
