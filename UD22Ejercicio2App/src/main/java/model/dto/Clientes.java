package model.dto;


public class Clientes {

	private Integer idCliente;
	private String nombreCliente;
	private String apellidoCliente;
	private String direccionCliente;
	private Integer dniCliente;
	private Integer fechaCliente;
	
	/**
	 * @return the idCliente
	 */
	public Integer getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public static void setIdCliente(Integer idCliente) {
		idCliente = idCliente;
	}
	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public static void setNombreCliente(String nombreCliente) {
		nombreCliente = nombreCliente;
	}
	/**
	 * @return the apellidoCliente
	 */
	public String getApellidoCliente() {
		return apellidoCliente;
	}
	/**
	 * @param apellidoCliente the apellidoCliente to set
	 */
	public static void setApellidoCliente(String apellidoCliente) {
		apellidoCliente = apellidoCliente;
	}
	/**
	 * @return the direccionCliente
	 */
	public String getDireccionCliente() {
		return direccionCliente;
	}
	/**
	 * @param direccionCliente the direccionCliente to set
	 */
	public static void setDireccionCliente(String direccionCliente) {
		direccionCliente = direccionCliente;
	}
	/**
	 * @return the dniCliente
	 */
	public Integer getdniCliente() {
		return dniCliente;
	}
	/**
	 * @param dniCliente the dniCliente to set
	 */
	public static void setdniCliente(Integer dniCliente) {
		dniCliente = dniCliente;
	}
	/**
	 * @return the fechaCliente
	 */
	public Integer getfechaCliente() {
		return fechaCliente;
	}
	/**
	 * @param fechaCliente the fechaCliente to set
	 */
	public static void setfechaCliente(Integer fechaCliente) {
		fechaCliente = fechaCliente;
	}

}
