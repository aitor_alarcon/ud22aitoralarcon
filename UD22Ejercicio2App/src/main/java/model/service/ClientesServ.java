package model.service;

import javax.swing.JOptionPane;

import controller.clienteController;
import model.dao.ClientesDao;
import model.dto.Clientes;

public class ClientesServ {

	private clienteController ClientesControllerRegistrar; 
	public static boolean consultaCliente=false;
	public static boolean modificaCliente=false;
	
	//Metodo de vinculaciÃ³n con el controller principal
	public void setpersonaController(clienteController personaController) {
		this.setController(ClientesControllerRegistrar);		
	}

	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Clientes miCliente) {
		ClientesDao miClienteDao;
		if (miCliente.getIdCliente() > 0) {
			miClienteDao = new ClientesDao();
			miClienteDao.registrarCliente(miCliente);						
		}else {
			JOptionPane.showMessageDialog(null,"El codigo de cliente ha de ser positivo","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
		
	}
	
	//Metodo que valida los datos de consulta antes de pasar estos al DAO
	public Clientes validarConsulta(String codigoCliente) {
		ClientesDao miClienteDao;
		
		try {
			int codigo=Integer.parseInt(codigoCliente);
			if (codigo > 0) {
				miClienteDao = new ClientesDao();
				consultaCliente=true;
				return miClienteDao.buscarCliente(codigo);						
			}else{
				JOptionPane.showMessageDialog(null,"El codigo de cliente ha de ser positivo","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaCliente=false;
			}
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaCliente=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaCliente=false;
		}
					
		return null;
	}

	//Metodo que valida los datos de ModificaciÃ³n antes de pasar estos al DAO
	public void validarModificacion(Clientes miCliente) {
		ClientesDao miClienteDao;
		if (miCliente.getNombreCliente().length()>0) {
			miClienteDao = new ClientesDao();
			miClienteDao.modificarCliente(miCliente);	
			modificaCliente=true;
		}else{
			JOptionPane.showMessageDialog(null,"El nombre de la persona debe ser mayor a 0 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificaCliente=false;
		}
	}

	//Metodo que valida los datos de EliminaciÃ³n antes de pasar estos al DAO
	public void validarEliminacion(String codigo) {
		ClientesDao miClienteDao=new ClientesDao();
		miClienteDao.eliminarPersona(codigo);
	}

	
	
	public clienteController getPersonaController() {
		return ClientesControllerRegistrar;
	}

	public void setController(clienteController ClientesControllerRegistrar) {
		this.ClientesControllerRegistrar = ClientesControllerRegistrar;
	}



}


