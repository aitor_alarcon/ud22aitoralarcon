package controller;

import model.dto.Clientes;
import model.service.ClientesServ;
import views.ventanaBuscarClientes;
import views.ventanaPrincipal;
import views.ventanaRegistrarClientes;

public class clienteController {

	private ClientesServ clientesServ;
	private ventanaPrincipal miVentanaPrincipal;
	private ventanaRegistrarClientes miVentanaRegistrarClientes;
	
	//Metodos getter Setters de vistas
	public ventanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	
	public void setMiVentanaPrincipal(ventanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	
	public ventanaRegistrarClientes getMiVentanaRegistro() {
		return miVentanaRegistrarClientes;
	}
	
	public void setMiVentanaRegistro(ventanaRegistrarClientes miVentanaRegistro) {
		this.miVentanaRegistrarClientes = miVentanaRegistro;
	}
	
	public ClientesServ getClientesServ() {
		return clientesServ;
	}
	public void setClientesServ(ClientesServ clientesServ) {
		this.clientesServ = clientesServ;
	}
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarVentanaRegistro() {
		miVentanaRegistrarClientes.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCliente(Clientes miCliente) {
		clientesServ.validarRegistro(miCliente);
	}
	
	public Clientes buscarCliente(String codigoCliente) {
		return clientesServ.validarConsulta(codigoCliente);
	}
	
	public void modificarCliente(Clientes miCliente) {
		clientesServ.validarModificacion(miCliente);
	}
	
	public void eliminarCliente(String codigoCliente) {
		clientesServ.validarEliminacion(codigoCliente);
	}


}

