package model.dto;

public class Videos {

	private Integer idVideo;
	private String tituloVideo;
	private String nombreDirectorVideo;
	
	/**
	 * @return the idVideo
	 */
	public Integer getIdVideo() {
		return idVideo;
	}
	/**
	 * @param idVideo the idVideo to set
	 */
	public void setIdCliente(Integer idVideo) {
		this.idVideo = idVideo;
	}
	/**
	 * @return the tituloVideo
	 */
	public String getTituloVideo() {
		return tituloVideo;
	}
	/**
	 * @param tituloVideo the tituloVideo to set
	 */
	public void setTituloVideo(String tituloVideo) {
		this.tituloVideo = tituloVideo;
	}
	/**
	 * @return the nombreDirectorVideo
	 */
	public String getNombreDirectorVideo() {
		return nombreDirectorVideo;
	}
	/**
	 * @param nombreDirectorVideo the nombreDirectorVideo to set
	 */
	public void setNombreDirectorVideo(String nombreDirectorVideo) {
		this.nombreDirectorVideo = nombreDirectorVideo;
	}

}
