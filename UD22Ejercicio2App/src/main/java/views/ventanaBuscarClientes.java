package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;

public class ventanaBuscarClientes extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldCodigo;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JLabel lblEdad;
	private JTextField textFieldDni;
	private JLabel lblDireccion;
	private JTextField textFieldDireccion;
	private JLabel lblFecha;
	private JTextField textFieldFecha;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JButton btnCancelar;
	private JLabel lblAdministrarClientes;

	public ventanaBuscarClientes() {
		setTitle("Administrar clientes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 646, 452);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Código");
		lblCodigo.setBounds(38, 105, 56, 16);
		contentPane.add(lblCodigo);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(38, 162, 56, 16);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(38, 226, 56, 16);
		contentPane.add(lblApellido);
		
		textFieldCodigo = new JTextField();
		textFieldCodigo.setBounds(98, 101, 116, 24);
		contentPane.add(textFieldCodigo);
		textFieldCodigo.setColumns(10);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(98, 159, 226, 24);
		contentPane.add(textFieldNombre);
		
		textFieldApellido = new JTextField();
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(98, 223, 226, 24);
		contentPane.add(textFieldApellido);
		
		lblEdad = new JLabel("DNI");
		lblEdad.setBounds(356, 162, 30, 16);
		contentPane.add(lblEdad);
		
		textFieldDni = new JTextField();
		textFieldDni.setColumns(10);
		textFieldDni.setBounds(399, 159, 178, 24);
		contentPane.add(textFieldDni);
		
		lblDireccion = new JLabel("Dirección");
		lblDireccion.setBounds(38, 282, 56, 16);
		contentPane.add(lblDireccion);
		
		textFieldDireccion = new JTextField();
		textFieldDireccion.setColumns(10);
		textFieldDireccion.setBounds(98, 279, 226, 24);
		contentPane.add(textFieldDireccion);
		
		lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(356, 226, 46, 16);
		contentPane.add(lblFecha);
		
		textFieldFecha = new JTextField();
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(399, 223, 178, 24);
		contentPane.add(textFieldFecha);
		
		JButton btnOk = new JButton("Ok");
		btnOk.setBounds(227, 101, 73, 25);
		contentPane.add(btnOk);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(126, 326, 109, 25);
		contentPane.add(btnGuardar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBounds(247, 326, 109, 25);
		contentPane.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(368, 326, 109, 25);
		contentPane.add(btnEliminar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(247, 364, 109, 25);
		contentPane.add(btnCancelar);
		
		lblAdministrarClientes = new JLabel("ADMINISTRAR CLIENTES");
		lblAdministrarClientes.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAdministrarClientes.setBounds(199, 40, 240, 32);
		contentPane.add(lblAdministrarClientes);
	}
}
